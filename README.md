# Codebase release 0.5 for mapyde

by Giordon Stark, Camila Aristimuno Ots, Mike Hance

SciPost Phys. Codebases 27-r0.5 (2024) - published 2024-02-28

[DOI:10.21468/SciPostPhysCodeb.27-r0.5](https://doi.org/10.21468/SciPostPhysCodeb.27-r0.5)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.27-r0.5) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Giordon Stark, Camila Aristimuno Ots, Mike Hance.

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.27-r0.5](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.27-r0.5)
* Live (external) repository at [https://github.com/scipp-atlas/mapyde/tree/v0.5.0](https://github.com/scipp-atlas/mapyde/tree/v0.5.0)
